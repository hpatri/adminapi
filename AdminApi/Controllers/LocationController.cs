﻿
using AdminCore.Location;
using AdminDTO.DTO;
using Microsoft.AspNetCore.Mvc;

namespace AdminApi.Controllers
{
    [ApiController]
    [Route("adminapi/[controller]")]
    public class LocationController : ControllerBase
    {
        private ILocationService locationService;
        public LocationController(ILocationService _locationService)
        {
            locationService = _locationService;
        }
        [HttpGet("{practiceId}")]
        public ActionResult<PracticeLocationDTO> GetLocations(int practiceId)
        {
            return Ok(locationService.GetPracticeLocations(practiceId));
        }
    }
}