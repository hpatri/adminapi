﻿using Microsoft.AspNetCore.Mvc;
using AdminCore.Practice;
using System.Collections.Generic;
using AdminDTO.DTO;
using System;

namespace AdminApi.Controllers
{
    [ApiController]
    [Route("adminapi/[controller]")]
    public class PracticesController : ControllerBase
    {
        private IPracticeService practiceService;
        public PracticesController(IPracticeService _practiceService) {
            practiceService = _practiceService;
        }

        [HttpGet]
        public ActionResult<IEnumerable<PracticeDTO>> GetAllPractices()
        {
            return Ok(practiceService.GetAllPractices());
        }

        [HttpGet("{id}")]
        public ActionResult<PracticeDTO> GetPracticeById(int id)
        {
            return Ok(practiceService.GetPracticeById(id));
        }

        [HttpGet]
        [Route("GetPracticeStatus")]
        public int GetPracticeStatus(int id)
        {
            return practiceService.GetPracticeStatus(id);
        }
        [HttpGet]
        [Route("GetPracticeTimezone")]
        public TimezoneDTO GetPracticeTimezone(int id)
        {
            return practiceService.GetPracticeTimezone(id);
        }


        //[HttpGet]
        //[Route("GetPracticeGroup")]
        //public TimezoneDTO GetPracticeTimezone(int id)
        //{
        //    return practiceService.GetPracticeTimezone(id);
        //}
        [HttpPost]
        [Route("UpdatePracticeStatus")]
        public ActionResult<PracticeDTO> UpdatePracticeStatus(int practiceId,int activeInd)
        {
            return practiceService.UpdatePracticeStatus(practiceId, activeInd);
        }


        [HttpPost]
        public ActionResult<PracticeDTO> CreatePractice(PracticeDTO practiceDTO) {

            practiceService.CreatePractice(practiceDTO);
            return Ok(practiceDTO);

        }
        [HttpPost]
        [Route("UpdatePractice")]
        public ActionResult<PracticeDTO> UpdatePractice( PracticeDTO practiceDTO)
        {
            practiceService.UpdatePractice(practiceDTO);
            return Ok(practiceDTO);
        }
        [HttpPost]
        [Route("UpdatePracticeEmr")]
        public PracticeDTO UpdatePracticeEmr(int practiceId ,int emrTypeId )
        {
            PracticeDTO practiceDTO = practiceService.UpdatePracticeEmr(practiceId, emrTypeId);
            return practiceDTO;
        }
        [HttpPost]
        [Route("UpdatePracticeAccountType")]
        public PracticeDTO UpdatePracticeAccountType(int practiceId, int accountTypeId)
        {
            PracticeDTO practiceDTO = practiceService.UpdatePracticeAccountType(practiceId, accountTypeId);
            return practiceDTO;
        }
        [HttpPost]
        [Route("UpdatePracticeSignup")]
        public PracticeDTO UpdatePracticeSignup(int practiceId,int legacyPracticeId,DateTime signupDate)
        {
            PracticeDTO practiceDTO = practiceService.UpdatePracticeSignup(practiceId, legacyPracticeId, signupDate);
            return practiceDTO;
        }
        
    }
}