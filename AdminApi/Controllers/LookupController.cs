﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AdminCore.Lookup;
using AdminDTO.DTO.Lookup;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace AdminApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class LookupController : ControllerBase
    {
        private ILookupService lookupService;
        public LookupController(ILookupService _lookupService)
        {
            lookupService = _lookupService;
        }

        [HttpGet]
        [Route("EmrTypes")]
        public ActionResult<IEnumerable<EmrTypeDTO>> GetEmrTypes()
        {
            return Ok(lookupService.GetEmrTypes());
        }

        [HttpGet]
        [Route("Groups")]
        public ActionResult<GroupDTO> GetGroups()
        {
            return Ok(lookupService.GetGroups());
        }
        [HttpGet]
        [Route("Skins")]
        public ActionResult<GroupDTO> GetSkins()
        {
            return Ok(lookupService.GetSkins());
        }
    }
}