﻿using AdminDTO.DTO.Lookup;
using System;
using System.Collections.Generic;
using System.Text;

namespace AdminCore.Lookup
{
  public  interface ILookupService
    {
        List<GroupDTO> GetGroups();
        List<EmrTypeDTO> GetEmrTypes();
        List<SkinDTO> GetSkins();
    }
}
