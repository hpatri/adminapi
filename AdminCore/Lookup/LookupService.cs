﻿using System;
using System.Collections.Generic;
using System.Text;
using AdminData.Lookup;
using AdminDTO.DTO.Lookup;
using AutoMapper;

namespace AdminCore.Lookup
{
   public class LookupService : ILookupService
    {
        private ILookupRepo  lookupRepo;
        private IMapper mapper;
        public LookupService(ILookupRepo repo, IMapper autoMapper)
        {
            lookupRepo = repo;
            mapper = autoMapper;
        }
        public List<EmrTypeDTO> GetEmrTypes()
        {
            return lookupRepo.GetEmrTypes();
        }

        public List<GroupDTO> GetGroups()
        {
            return lookupRepo.GetGroups();
        }

        public List<SkinDTO> GetSkins()
        {
            return lookupRepo.GetSkins();
        }
    }
}
