﻿using System;
using System.Collections.Generic;
using AdminDTO.DTO;
using Microsoft.AspNetCore.Mvc;

namespace AdminCore.Practice
{
    public interface IPracticeService
    {
        IEnumerable<PracticeDTO> GetAllPractices();
        PracticeDTO GetPracticeById(int PracticeId);
        PracticeDTO CreatePractice(PracticeDTO practiceDTO);
        PracticeDTO UpdatePractice(PracticeDTO practiceDTO);
        PracticeDTO UpdatePracticeEmr(int practiceId, int emrTypeId);
        PracticeDTO UpdatePracticeAccountType(int practiceId, int emrTypeId);
        PracticeDTO UpdatePracticeSignup(int practiceId, int legacyPracticeId, DateTime signupDate);
        //PracticeLocationDTO GetLocations(int practiceId);
        int GetPracticeStatus(int id);
        ActionResult<PracticeDTO> UpdatePracticeStatus(int id, int activeInd);
        TimezoneDTO GetPracticeTimezone(int id);
    }
}