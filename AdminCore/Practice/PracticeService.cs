﻿using AdminData.Location;
using AdminData.Practice;
using AdminDTO.DTO;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Text;


namespace AdminCore.Practice
{
 public   class PracticeService:IPracticeService
    {
        private IPracticeRepo practiceRepo;
        private ILocationRepo locationRepo;
        private IMapper mapper;
        public PracticeService(IPracticeRepo repo,ILocationRepo locRepo,IMapper autoMapper)
        {
         practiceRepo = repo;
         locationRepo=locRepo;
         mapper = autoMapper;
        }
        public AdminDTO.DTO.PracticeDTO GetPracticeById(int practiceId)
        {
            return practiceRepo.GetPracticeById(practiceId);
        }

        public IEnumerable<AdminDTO.DTO.PracticeDTO> GetAllPractices()
        {
            return practiceRepo.GetAllPractices();
        }
        public PracticeDTO CreatePractice(PracticeDTO practiceDTO)
        { if (practiceDTO == null)
                throw new ArgumentNullException(nameof(practiceDTO));
            
            return practiceRepo.CreatePractice(practiceDTO);
        }
        public PracticeDTO UpdatePractice(PracticeDTO practiceDTO)
        {
            if (practiceDTO == null)
                throw new ArgumentNullException(nameof(practiceDTO));

            return practiceRepo.UpdatePractice(practiceDTO);
        }

        public PracticeDTO UpdatePracticeEmr(int practiceId, int emrTypeId)
        {
            return practiceRepo.UpdatePracticeEmr( practiceId,  emrTypeId);
        }

        public PracticeDTO UpdatePracticeAccountType(int practiceId, int emrTypeId)
        {
            return practiceRepo.UpdatePracticeAccountType(practiceId, emrTypeId);
        }
        public PracticeDTO UpdatePracticeSignup(int practiceId, int legacyPracticeId, DateTime signupDate)
        {
            return practiceRepo.UpdatePracticeSignup(practiceId, legacyPracticeId,signupDate);
        }

      

        public int GetPracticeStatus(int id)
        {
            return practiceRepo.GetPracticeStatus(id);
        }

        public ActionResult<PracticeDTO> UpdatePracticeStatus(int id,int activeInd)
        {
            return practiceRepo.UpdatePracticeStatus(id, activeInd);
        }

        public TimezoneDTO GetPracticeTimezone(int id)
        {
            return practiceRepo.GetPracticeTimezone(id);
        }
    }
}
