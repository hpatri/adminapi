﻿using System;
using System.Collections.Generic;
using System.Text;
using AdminData.Location;
using AdminData.Practice;
using AdminDTO.DTO;
using AutoMapper;

namespace AdminCore.Location
{
    public class LocationService : ILocationService
    {
        private ILocationRepo locationRepo;
        private IPracticeRepo practiceRepo;
        private IMapper mapper;
     
        public LocationService(ILocationRepo _locationRepo, IMapper autoMapper,IPracticeRepo _practiceRepo)
        {
            locationRepo = _locationRepo;
            practiceRepo = _practiceRepo;
            mapper = autoMapper;
        }

        //public List<LocationDTO> GetLocations(int practiceId)
        //{
        //    throw new NotImplementedException();
        //}

        public PracticeLocationDTO GetPracticeLocations(int practiceId)
        {
           
            PracticeDTO practiceDTO = new PracticeDTO();
            PracticeLocationDTO practiceLocationDTO = new PracticeLocationDTO();

            practiceDTO = practiceRepo.GetPracticeById(practiceId);
            practiceLocationDTO.id = practiceDTO.id;
            practiceLocationDTO.legacy = practiceDTO.legacy;
            practiceLocationDTO.uuId = practiceDTO.uuId;
            practiceLocationDTO.name = practiceDTO.name;
            practiceLocationDTO.displayName = practiceDTO.displayName;
            practiceLocationDTO.emrTypeId = practiceDTO.emrTypeId;
            practiceLocationDTO.acctNum = practiceDTO.acctNum;
            practiceLocationDTO.acctMgrId = practiceDTO.acctMgrId;
            practiceLocationDTO.legacyPID = practiceDTO.legacyPID;
            practiceLocationDTO.legacy = practiceDTO.legacy;
            practiceLocationDTO.active = practiceDTO.active;
            practiceLocationDTO.accountType = practiceDTO.accountType;
            practiceLocationDTO.supportLevelId = practiceDTO.supportLevelId;
            practiceLocationDTO.signupDate = practiceDTO.signupDate;
            practiceLocationDTO.locationsOwned = practiceDTO.locationsOwned;
            practiceLocationDTO.locationsSigned = practiceDTO.locationsSigned;
            practiceLocationDTO.websiteUrl = practiceDTO.websiteUrl;
            practiceLocationDTO.abbRepLoginId = practiceDTO.abbRepLoginId;
            practiceLocationDTO.groupId = practiceDTO.groupId;
            practiceLocationDTO.contactUserId = practiceDTO.contactUserId;
            

            //practiceLocationDTO = mapper.Map<PracticeLocationDTO>(practiceDTO);

            practiceLocationDTO.locations = locationRepo.GetLocations(practiceId);
           
            return practiceLocationDTO;
        }
    }
}
