﻿using AdminDTO.DTO;
using System;
using System.Collections.Generic;
using System.Text;

namespace AdminCore.Location
{
    public interface ILocationService
    {
        PracticeLocationDTO GetPracticeLocations(int practiceId);
    }
}
