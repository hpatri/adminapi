﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AdminDTO.DTO
{
    class AccountManagerDTO
    {
        public int id { get; set; }
        public string firstName { get; set; }
        public string lastName { get; set; }
        public string email { get; set; }
        public string phone { get; set; }
        public string phoneExt { get; set; }
        public int active { get; set; }
        public int title { get; set; }
        public string discoveryUrl { get; set; }
        public string reviewUrl { get; set; }
    }
}
