﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AdminDTO.DTO.Lookup
{
   public class GroupDTO :LookupDTO
    {
        //public int id { get; set; }
        //public string name { get; set; }
        public string displayName { get; set; }
        public int active { get; set; }
        public int skinId { get; set; }
        public int averageId { get; set; }
        public int industryId { get; set; }
        public int externalVisible { get; set; }
    }
}
