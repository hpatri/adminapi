﻿using AdminDTO.DTO.Lookup;
using System;
using System.Collections.Generic;
using System.Text;

namespace AdminDTO.DTO.Lookup { 
  public  class EmrTypeDTO:LookupDTO
    {
        //public int id { get; set; }
        //public string name { get; set; }
        public string product { get; set; }
        public int active { get; set; }
        public string emrUrl { get; set; }
    }
}
