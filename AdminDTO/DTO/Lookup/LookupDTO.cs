﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AdminDTO.DTO.Lookup
{
  public  class LookupDTO
    {
        public int id { get; set; }
        public string name { get; set; }
    }
}
