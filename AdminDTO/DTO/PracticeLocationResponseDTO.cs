﻿using System.Collections.Generic;

namespace AdminDTO.DTO
{
    public class PracticeLocationDTO : PracticeDTO
    {
          public List<LocationDTO> locations { get; set; }  = new List<LocationDTO>();
       
    }
}
