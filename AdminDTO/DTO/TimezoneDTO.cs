﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AdminDTO.DTO
{
   public class TimezoneDTO
    {
        public int practiceId { get; set; }
        public string zone { get; set; }
        public string state { get; set; }

    }
}
