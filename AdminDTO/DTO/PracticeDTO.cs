﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AdminDTO.DTO
{
  public  class PracticeDTO
    {
       
        public int id { get; set; }
        public string uuId { get; set; }
        public string name { get; set; }
        public string displayName { get; set; }
        public int emrTypeId { get; set; }
        public int acctNum { get; set; }
        public int acctMgrId { get; set; }
        //public string acctMgrEmail { get; set; }
        public int legacyPID { get; set; }
        public int legacy { get; set; }
        public int active { get; set; }
        public int accountType { get; set; }
        public int supportLevelId { get; set; }
        public DateTime signupDate { get; set; }
        public int locationsOwned { get; set; }
        public int locationsSigned { get; set; }
        public string websiteUrl { get; set; }
        public int abbRepLoginId { get; set; }
        public int groupId { get; set; }
        public int contactUserId { get; set; }

    }
}
