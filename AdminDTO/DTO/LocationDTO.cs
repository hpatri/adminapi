﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AdminDTO.DTO
{
    public class LocationDTO
    {
       

        // public DateTime maxBillingDate { get; set; } = DateTime.MinValue;

        public int id { get; set; } = 0;
        public string name { get; set; } = String.Empty;
        public string displayName { get; set; } = String.Empty;
        public string officePhone { get; set; } = String.Empty;
        public int templateId { get; set; } = 0;
        public int accountExt { get; set; } = 0;
        public string city { get; set; } = String.Empty;
        public string state { get; set; } = String.Empty;
        public int practiceStatus { get; set; } = 0;
        public int active { get; set; } = 0;

        //public string uuid { get; set; } = String.Empty;
        //public int practiceId { get; set; } = 0;

        //public int typeId { get; set; } = 0;
        //    public string homePage { get; set; } = String.Empty;
        //    public string path { get; set; } = String.Empty;
        //    public int industryId { get; set; } = 0;

        //    public int affiliationId { get; set; } = 0;
        //    public string cssFile { get; set; } = String.Empty;
        //    public int groupId { get; set; } = 0;
        //    public int openProcessId { get; set; } = 0;

    }
}
