﻿using AdminDTO.DTO;
using AdminDTO.DTO.Lookup;
using System;
using System.Collections.Generic;
using System.Text;

namespace AdminData.Lookup
{
   public interface ILookupRepo
    {
        List<GroupDTO> GetGroups();
        List<EmrTypeDTO> GetEmrTypes();
        List<SkinDTO> GetSkins();
    }
}


