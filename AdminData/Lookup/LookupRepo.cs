﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Text;
using AdminDTO.DTO;
using AdminDTO.DTO.Lookup;
using Microsoft.Extensions.Configuration;

namespace AdminData.Lookup
{
public  class LookupRepo : ILookupRepo
    {
        public IConfiguration _Connectionstring;
        public String ConnString;

        //TODO Move Stored Procs to central location

        private const string getEmrTypeList = "asp_getEmrList";
        private const string getGroupList = "asp_getGroupList";
        private const string getSkinList = "asp_getSkinList";

        public LookupRepo(IConfiguration configuration)
        {
            _Connectionstring = configuration;
            ConnString = ConnectionStrings.GetConnectionString(_Connectionstring);
        }
        public List<EmrTypeDTO> GetEmrTypes()
        {
            List<EmrTypeDTO> emrTypesList = new List<EmrTypeDTO>();

            using (SqlConnection conn = new SqlConnection(ConnString))
            {
                using (SqlCommand cmd = new SqlCommand(getEmrTypeList, conn))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    conn.Open();
                    using (SqlDataReader reader = cmd.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            EmrTypeDTO emrTypeDTO = new EmrTypeDTO();
                            emrTypeDTO.id = reader.IsDBNull(reader.GetOrdinal("id")) ? 0 : reader.GetInt32(reader.GetOrdinal("id"));
                            emrTypeDTO.active = reader.IsDBNull(reader.GetOrdinal("active")) ? 0 : reader.GetInt32(reader.GetOrdinal("active"));
                            emrTypeDTO.emrUrl = reader.IsDBNull(reader.GetOrdinal("emrUrl")) ? string.Empty : reader.GetString(reader.GetOrdinal("emrUrl"));
                            emrTypeDTO.product = reader.IsDBNull(reader.GetOrdinal("product")) ? string.Empty : reader.GetString(reader.GetOrdinal("product"));
                            emrTypeDTO.name= reader.IsDBNull(reader.GetOrdinal("name")) ? string.Empty : reader.GetString(reader.GetOrdinal("name"));
                            emrTypesList.Add(emrTypeDTO);
                        }
                    }
                }
            }
            return emrTypesList;
        }

      
        public List<GroupDTO> GetGroups()
        {
            List<GroupDTO> groupList = new List<GroupDTO>();
            using (SqlConnection conn = new SqlConnection(ConnString))
            {
                using (SqlCommand cmd = new SqlCommand(getGroupList, conn))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    conn.Open();
                    using (SqlDataReader reader = cmd.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            GroupDTO groupDTO = new GroupDTO();
                            groupDTO.id = reader.IsDBNull(reader.GetOrdinal("id")) ? 0 : reader.GetInt32(reader.GetOrdinal("id"));
                            groupDTO.active = reader.IsDBNull(reader.GetOrdinal("active")) ? 0 : reader.GetInt32(reader.GetOrdinal("active"));
                            groupDTO.skinId= reader.IsDBNull(reader.GetOrdinal("skinId")) ? 0 : reader.GetInt32(reader.GetOrdinal("skinId"));
                            groupDTO.industryId= reader.IsDBNull(reader.GetOrdinal("industryId")) ? 0 : reader.GetInt32(reader.GetOrdinal("industryId"));
                            groupDTO.averageId = reader.IsDBNull(reader.GetOrdinal("averageId")) ? 0 : reader.GetInt32(reader.GetOrdinal("averageId"));
                            groupDTO.externalVisible = reader.IsDBNull(reader.GetOrdinal("externalVisible")) ? 0 : reader.GetInt32(reader.GetOrdinal("externalVisible"));
                            groupDTO.name = reader.IsDBNull(reader.GetOrdinal("name")) ? string.Empty: reader.GetString(reader.GetOrdinal("name"));
                            groupList.Add(groupDTO);
                        }
                    }
                }
            }
            return groupList;
        }

        public List<SkinDTO> GetSkins()
        {
            List<SkinDTO> skinList = new List<SkinDTO>();
            using (SqlConnection conn = new SqlConnection(ConnString))
            {
                using (SqlCommand cmd = new SqlCommand(getSkinList, conn))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    conn.Open();
                    using (SqlDataReader reader = cmd.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            SkinDTO skinDTO = new SkinDTO();
                            skinDTO.id = reader.IsDBNull(reader.GetOrdinal("id")) ? 0 : reader.GetInt32(reader.GetOrdinal("id"));
                            skinDTO.cssFile = reader.IsDBNull(reader.GetOrdinal("cssFile")) ? String.Empty: reader.GetString(reader.GetOrdinal("cssFile"));
                            skinDTO.name = reader.IsDBNull(reader.GetOrdinal("name")) ? string.Empty : reader.GetString(reader.GetOrdinal("name"));
                            skinList.Add(skinDTO);
                        }
                    }
                }
            }
            return skinList;
        }
    }
}
