﻿using AdminDTO.DTO;
using System.Collections.Generic;

namespace AdminData.Location
{
    public interface ILocationRepo
    {
        List<LocationDTO> GetLocations(int practiceId);
    }
}
