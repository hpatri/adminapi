﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Text;
using AdminDTO.DTO;
using Microsoft.Extensions.Configuration;

namespace AdminData.Location
{
    public class LocationRepo : ILocationRepo
    {
        public IConfiguration _Connectionstring;
        public String ConnString;
       
        //Stored Procs
        private const string getLocations = "asp_getLocationsForPractice";
        public LocationRepo(IConfiguration configuration)
        {
            _Connectionstring = configuration;
            ConnString = ConnectionStrings.GetConnectionString(_Connectionstring);
        }
        public List<LocationDTO> GetLocations(int practiceId)
        {
            List<LocationDTO> locations = new List<LocationDTO>();
         

            using (SqlConnection conn = new SqlConnection(ConnString))
            {
                using (SqlCommand cmd = new SqlCommand(getLocations, conn))
                {
                    cmd.Parameters.Add("@id", SqlDbType.VarChar).Value = practiceId;
                    cmd.CommandType = CommandType.StoredProcedure;
                    conn.Open();
                    using (SqlDataReader reader = cmd.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            LocationDTO location = new LocationDTO();
                            location.id = reader.IsDBNull(reader.GetOrdinal("id")) ? 0 : reader.GetInt32(reader.GetOrdinal("id"));
                            location.displayName = reader.IsDBNull(reader.GetOrdinal("displayName")) ? String.Empty : reader.GetString(reader.GetOrdinal("displayName"));
                            location.name = reader.IsDBNull(reader.GetOrdinal("name")) ? String.Empty : reader.GetString(reader.GetOrdinal("name"));
                            location.practiceStatus= reader.IsDBNull(reader.GetOrdinal("practiceStatus")) ? 0: reader.GetInt32(reader.GetOrdinal("practiceStatus"));
                            location.state = reader.IsDBNull(reader.GetOrdinal("state")) ? String.Empty : reader.GetString(reader.GetOrdinal("state"));
                            location.state = reader.IsDBNull(reader.GetOrdinal("city")) ? String.Empty : reader.GetString(reader.GetOrdinal("city"));
                            location.state = reader.IsDBNull(reader.GetOrdinal("officePhone")) ? String.Empty : reader.GetString(reader.GetOrdinal("officePhone"));
                            locations.Add(location);
                        }
                    }
                }
            }
            return locations;
        }
    }
}
