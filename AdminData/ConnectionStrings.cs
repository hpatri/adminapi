﻿using Microsoft.Extensions.Configuration;


namespace AdminData
{
   static  class ConnectionStrings
    {
        public static string GetConnectionString(IConfiguration Connectionstring)
        {
            string Db =  Connectionstring.GetSection("DbToConnect").GetSection("DB").Value;
            string ConnString = Connectionstring.GetSection("ConnectionStrings").GetSection(Db).Value;
            return ConnString;
        
        }
    }
}
