﻿using System;
using System.Collections.Generic;
using System.Text;
using AdminDTO.DTO;
using Microsoft.AspNetCore.Mvc;

namespace AdminData.Practice
{
   public class MockPracticeRepo :IPracticeRepo
    {
        public AdminDTO.DTO.PracticeDTO GetPracticeById(int PracticeId)
        {
            return new AdminDTO.DTO.PracticeDTO
            {
                id = 1,
                name = "moc practice",
                displayName = "mock display",
                emrTypeId = 2,
                acctNum = 3,
                acctMgrId = 2,
                //acctMgrEmail = "moc email",
                legacyPID = 2,
                active = 1,
                accountType = 2,
                supportLevelId = 3,
                signupDate = DateTime.MinValue,
                locationsOwned = 1,
                locationsSigned = 2,
                websiteUrl = "some url",
                abbRepLoginId = 3
            };
        }
        public IEnumerable<AdminDTO.DTO.PracticeDTO> GetAllPractices()
        {
            var practices = new List<AdminDTO.DTO.PracticeDTO> {
          new AdminDTO.DTO.PracticeDTO{id=1,name="moc practice",displayName ="mock display",emrTypeId=2,acctNum=3,
         acctMgrId =2,
             // acctMgrEmail ="moc email",
              legacyPID=2,active=1,accountType=2, supportLevelId=3,signupDate =DateTime.MinValue,
         locationsOwned =1,  locationsSigned =2, websiteUrl="some url", abbRepLoginId =3} };

            return practices;
        }

        public PracticeDTO  CreatePractice(PracticeDTO practiceDTO)
        {
            throw new NotImplementedException();
        }

        public PracticeDTO UpdatePractice(PracticeDTO practiceDTO)
        {
            throw new NotImplementedException();
        }

        public PracticeDTO UpdatePracticeEmr(int practiceId, int emrTypeId)
        {
            throw new NotImplementedException();
        }

        public PracticeDTO UpdatePracticeAccountType(int practiceId, int emrTypeId)
        {
            throw new NotImplementedException();
        }

        public PracticeDTO UpdatePracticeSignup(int practiceId, int legacyPracticeId, DateTime signupDate)
        {
            throw new NotImplementedException();
        }

        public PracticeDTO UpdatePracticeSignupDate(int practiceId, int legacyPracticeId, DateTime signupDate)
        {
            throw new NotImplementedException();
        }

        public int GetPracticeStatus(int practiceId)
        {
            throw new NotImplementedException();
        }

        public ActionResult<PracticeDTO> UpdatePracticeStatus(int id, int activeInd)
        {
            throw new NotImplementedException();
        }

        public TimezoneDTO GetPracticeTimezone(int id)
        {
            throw new NotImplementedException();
        }
    }
}
