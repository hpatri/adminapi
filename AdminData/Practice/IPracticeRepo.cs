﻿using AdminDTO.DTO;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Text;

namespace AdminData.Practice
{
  public interface IPracticeRepo
    {
         PracticeDTO GetPracticeById(int PracticeId);
         IEnumerable<PracticeDTO> GetAllPractices();
        PracticeDTO CreatePractice(PracticeDTO practiceDTO);
        PracticeDTO UpdatePractice(PracticeDTO practiceDTO);
        PracticeDTO UpdatePracticeEmr(int practiceId, int emrTypeId);
        PracticeDTO UpdatePracticeAccountType(int practiceId, int emrTypeId);
        PracticeDTO UpdatePracticeSignup(int practiceId, int legacyPracticeId, DateTime signupDate);
        int GetPracticeStatus(int practiceId);
        ActionResult<PracticeDTO> UpdatePracticeStatus(int id, int activeInd);
        TimezoneDTO GetPracticeTimezone(int id);
    }
}
