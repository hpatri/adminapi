﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Text;
using AdminData.Practice;
using AdminDTO.DTO;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;

namespace AdminData.Practice
{
    public class PracticeRepo : IPracticeRepo
    {
        public IConfiguration _Connectionstring;
        public String ConnString;
        private SqlDataReader reader;
        

        //TODO Move Stored Procs to central location
        private const string getPracticeList = "asp_getPracticeList";
        private const string getPracticeById = "asp_getPracticeById";
        private const string getPracticeTimezone = "asp_getPracticeTimezone";
        private const string getPracticeStatus = "asp_getPracticeStatus";
        private const string createPractice = "asp_createPractice";
        private const string updatePractice = "asp_updatePractice";
        private const string updatePracticeStatus = "asp_updatePracticeStatus";
        private const string updatePracticeEmr = "asp_updatePracticeEmr";
        private const string updatePracticeAccountType = "asp_updatePracticeAccountType";
        private const string updatePracticeSignup = "asp_updatePracticeSignup";
     

        public PracticeRepo(IConfiguration configuration)
        {
            _Connectionstring = configuration;
            ConnString = ConnectionStrings.GetConnectionString(_Connectionstring);
        }
        public PracticeDTO GetPracticeById(int practiceId)
        {
            PracticeDTO practiceDTO = new PracticeDTO();

            using (SqlConnection conn = new SqlConnection(ConnString))
            {
                using (SqlCommand cmd = new SqlCommand(getPracticeById, conn))
                {
                    cmd.Parameters.Add("@id", SqlDbType.VarChar).Value = practiceId;
                    cmd.CommandType = CommandType.StoredProcedure;
                    conn.Open();
                    using (SqlDataReader reader = cmd.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            MapReaderToDTO(reader, practiceDTO);
                        }
                    }
                }
            }
            return practiceDTO;
        }
        public TimezoneDTO GetPracticeTimezone(int practiceId)
        {
            TimezoneDTO timezone = new TimezoneDTO();

            using (SqlConnection conn = new SqlConnection(ConnString))
            {
                using (SqlCommand cmd = new SqlCommand(getPracticeTimezone, conn))
                {
                    cmd.Parameters.Add("@id", SqlDbType.VarChar).Value = practiceId;
                    cmd.CommandType = CommandType.StoredProcedure;
                    conn.Open();
                    using (SqlDataReader reader = cmd.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            timezone.practiceId= reader.IsDBNull(reader.GetOrdinal("id")) ? 0 : reader.GetInt32(reader.GetOrdinal("id"));
                            timezone.zone= reader.IsDBNull(reader.GetOrdinal("timeZone")) ? String.Empty : reader.GetString(reader.GetOrdinal("timeZone"));
                            timezone.state= reader.IsDBNull(reader.GetOrdinal("timeState")) ? String.Empty : reader.GetString(reader.GetOrdinal("timeState"));
                        }
                    }
                }
            }
            return timezone;
        }
        public IEnumerable<PracticeDTO> GetAllPractices()
        {
            List<PracticeDTO> practiceDTOs = new List<PracticeDTO>();
            using (SqlConnection conn = new SqlConnection(ConnString))
            {
                using (SqlCommand cmd = new SqlCommand(getPracticeList, conn))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    conn.Open();
                    using (SqlDataReader reader = cmd.ExecuteReader())
                    {
                        if (reader.HasRows)
                        {
                            while (reader.Read())
                            {
                                PracticeDTO practiceDTO = new PracticeDTO();
                                MapReaderToDTO( reader, practiceDTO);
                                practiceDTOs.Add(practiceDTO);
                            }
                        }
                    }
                }
            }
            return practiceDTOs;
        }
        private void MapReaderToDTO( SqlDataReader reader, PracticeDTO practiceDTO)
        {
	        practiceDTO.id = reader.IsDBNull(reader.GetOrdinal("id")) ? 0 : reader.GetInt32(reader.GetOrdinal("id"));
           practiceDTO.uuId = reader.IsDBNull(reader.GetOrdinal("uuid")) ? String.Empty : reader.GetString(reader.GetOrdinal("uuid"));
            practiceDTO.name = reader.IsDBNull(reader.GetOrdinal("name")) ? String.Empty : reader.GetString(reader.GetOrdinal("name"));
            practiceDTO.displayName = reader.IsDBNull(reader.GetOrdinal("displayName")) ? String.Empty : reader.GetString(reader.GetOrdinal("displayName"));
            practiceDTO.emrTypeId = reader.IsDBNull(reader.GetOrdinal("emrTypeId")) ? 0 : reader.GetInt32(reader.GetOrdinal("emrTypeId"));
            practiceDTO.acctNum = reader.IsDBNull(reader.GetOrdinal("accountNumber")) ? 0 : reader.GetInt32(reader.GetOrdinal("accountNumber"));
            practiceDTO.accountType = reader.IsDBNull(reader.GetOrdinal("accountTypeId")) ? 0 : reader.GetInt32(reader.GetOrdinal("accountTypeId"));
            practiceDTO.supportLevelId = reader.IsDBNull(reader.GetOrdinal("supportLevelId")) ? 0 : reader.GetInt32(reader.GetOrdinal("supportLevelId"));
            practiceDTO.acctMgrId = reader.IsDBNull(reader.GetOrdinal("accountManagerId")) ? 0 : reader.GetInt32(reader.GetOrdinal("accountManagerId"));
            practiceDTO.legacy = reader.IsDBNull(reader.GetOrdinal("legacy")) ? 0 : reader.GetInt32(reader.GetOrdinal("legacy"));
            practiceDTO.legacyPID = reader.IsDBNull(reader.GetOrdinal("legacyPracticeId")) ? 0 : reader.GetInt32(reader.GetOrdinal("legacyPracticeId"));
            practiceDTO.locationsOwned = reader.IsDBNull(reader.GetOrdinal("locationsOwned")) ? 0 : reader.GetInt32(reader.GetOrdinal("locationsOwned"));
            practiceDTO.locationsSigned = reader.IsDBNull(reader.GetOrdinal("locationsSigned")) ? 0 : reader.GetInt32(reader.GetOrdinal("locationsSigned"));
            practiceDTO.websiteUrl = reader.IsDBNull(reader.GetOrdinal("websiteUrl")) ? String.Empty : reader.GetString(reader.GetOrdinal("websiteUrl"));
            practiceDTO.abbRepLoginId = reader.IsDBNull(reader.GetOrdinal("abbRepLoginId")) ? 0 : reader.GetInt32(reader.GetOrdinal("abbRepLoginId"));
            practiceDTO.active = reader.IsDBNull(reader.GetOrdinal("active")) ? 0 : reader.GetInt32(reader.GetOrdinal("active"));
            //TODO Remove hardcoding practiceDTO.signupDate = reader.GetDateTime(reader.GetOrdinal("signupDate")); 
            practiceDTO.signupDate = DateTime.MinValue;
            practiceDTO.groupId= reader.IsDBNull(reader.GetOrdinal("groupId")) ? 0 : reader.GetInt32(reader.GetOrdinal("groupId"));
            practiceDTO.contactUserId = reader.IsDBNull(reader.GetOrdinal("contactUserId")) ? 0 : reader.GetInt32(reader.GetOrdinal("contactUserId"));
        }
        public PracticeDTO CreatePractice(PracticeDTO practiceDTO)
        {
            using (SqlConnection conn = new SqlConnection(ConnString))
            {
                using (SqlCommand cmd = new SqlCommand(createPractice, conn))
                { 
                    cmd.CommandType = CommandType.StoredProcedure;
                    conn.Open();
                    BindObjectToParameters(cmd, practiceDTO);
                    reader = cmd.ExecuteReader();
                    if (reader.HasRows)
                    {
                        while (reader.Read())
                        {
                            MapReaderToDTO(reader, practiceDTO);
                        }
                    }
                    reader.Close();
                }
            }
            return practiceDTO;
        }
        public PracticeDTO UpdatePractice( PracticeDTO practiceDTO)
        {
            using (SqlConnection conn = new SqlConnection(ConnString))
            {
                using (SqlCommand cmd = new SqlCommand(updatePractice, conn))
                {
                    BindObjectToParameters(cmd, practiceDTO);

                    cmd.CommandType = CommandType.StoredProcedure;
                    conn.Open();
                    reader = cmd.ExecuteReader();
                    if (reader.HasRows)
                    {
                        while (reader.Read())
                        {
                            MapReaderToDTO(reader, practiceDTO);
                        }
                    }
                    reader.Close();
                }
            }
            return practiceDTO;
        }
        private void BindObjectToParameters( SqlCommand cmd, PracticeDTO practiceDTO)
        {
            cmd.Parameters.Add("@id", SqlDbType.Int).Value = practiceDTO.id;
            cmd.Parameters.Add("@uuid", SqlDbType.VarChar).Value = practiceDTO.uuId;
            cmd.Parameters.Add("@name", SqlDbType.VarChar).Value = practiceDTO.name;
            cmd.Parameters.Add("@displayName", SqlDbType.VarChar).Value = practiceDTO.displayName;
            cmd.Parameters.Add("@active", SqlDbType.Int).Value = practiceDTO.active;
            cmd.Parameters.Add("@legacy", SqlDbType.Int).Value = practiceDTO.legacy;
            cmd.Parameters.Add("@legacyPracticeId", SqlDbType.Int).Value = practiceDTO.legacyPID;
            cmd.Parameters.Add("@accountManagerId", SqlDbType.Int).Value = practiceDTO.acctMgrId;
            cmd.Parameters.Add("@emrTypeId", SqlDbType.Int).Value = practiceDTO.emrTypeId;
            cmd.Parameters.Add("@groupId", SqlDbType.Int).Value = practiceDTO.groupId;
            cmd.Parameters.Add("@locationsOwned", SqlDbType.Int).Value = practiceDTO.locationsOwned;
            cmd.Parameters.Add("@locationsSigned", SqlDbType.Int).Value = practiceDTO.locationsSigned;
            cmd.Parameters.Add("@abbRepLoginId", SqlDbType.Int).Value = practiceDTO.abbRepLoginId;
            cmd.Parameters.Add("@websiteUrl", SqlDbType.VarChar).Value = practiceDTO.websiteUrl;
            cmd.Parameters.Add("@accountTypeId", SqlDbType.Int).Value = practiceDTO.accountType;
            cmd.Parameters.Add("@signupDate", SqlDbType.Date).Value = practiceDTO.signupDate;
            cmd.Parameters.Add("@supportLevelId", SqlDbType.Int).Value = practiceDTO.supportLevelId;
            cmd.Parameters.Add("@acctNum", SqlDbType.Int).Value = practiceDTO.acctNum;
            cmd.Parameters.Add("@contactUserId", SqlDbType.Int).Value = practiceDTO.contactUserId;
        }

        public PracticeDTO UpdatePracticeEmr(int practiceId, int emrTypeId)
        {
            PracticeDTO practiceDTO = new PracticeDTO();
            using (SqlConnection conn = new SqlConnection(ConnString))
            {
                using (SqlCommand cmd = new SqlCommand(updatePracticeEmr, conn))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add("@id", SqlDbType.Int).Value = practiceId;
                    cmd.Parameters.Add("@emrTypeId", SqlDbType.Int).Value = emrTypeId;
                    conn.Open();


                    reader = cmd.ExecuteReader();
                    if (reader.HasRows)
                    {
                        while (reader.Read())
                        {
                            MapReaderToDTO(reader, practiceDTO);
                        }
                    }
                    reader.Close();
                }
            }
            return practiceDTO;
        }

        public PracticeDTO UpdatePracticeAccountType(int practiceId, int accountTypeId)
        {
            PracticeDTO practiceDTO = new PracticeDTO();
            using (SqlConnection conn = new SqlConnection(ConnString))
            {
                using (SqlCommand cmd = new SqlCommand(updatePracticeAccountType, conn))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add("@id", SqlDbType.Int).Value = practiceId;
                    cmd.Parameters.Add("@accountTypeId", SqlDbType.Int).Value = accountTypeId;
                    conn.Open();
                    reader = cmd.ExecuteReader();
                    if (reader.HasRows)
                    {
                        while (reader.Read())
                        {
                            MapReaderToDTO(reader, practiceDTO);
                        }
                    }
                    reader.Close();
                }
            }
            return practiceDTO;
        }
        public PracticeDTO UpdatePracticeSignup(int practiceId, int legacyPracticeId, DateTime signupDate)
        {
            PracticeDTO practiceDTO = new PracticeDTO();
            using (SqlConnection conn = new SqlConnection(ConnString))
            {
                using (SqlCommand cmd = new SqlCommand(updatePracticeSignup, conn))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add("@id", SqlDbType.Int).Value = practiceId;
                    cmd.Parameters.Add("@legacyPracticeId", SqlDbType.Int).Value = legacyPracticeId;
                    cmd.Parameters.Add("@signupDate", SqlDbType.DateTime).Value = signupDate;
                    conn.Open();


                    reader = cmd.ExecuteReader();
                    if (reader.HasRows)
                    {
                        while (reader.Read())
                        {
                            MapReaderToDTO(reader, practiceDTO);
                        }
                    }
                    reader.Close();
                }
            }
            return practiceDTO;
        }

        public int GetPracticeStatus(int practiceId)
        {
            int active = 0;
            using (SqlConnection conn = new SqlConnection(ConnString))
            {
                using (SqlCommand cmd = new SqlCommand(getPracticeStatus, conn))
                {
                    cmd.Parameters.Add("@id", SqlDbType.VarChar).Value = practiceId;
                    cmd.CommandType = CommandType.StoredProcedure;
                    conn.Open();
                    using (SqlDataReader reader = cmd.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            active= reader.IsDBNull(reader.GetOrdinal("active")) ? 0 : reader.GetInt32(reader.GetOrdinal("active"));
                        }
                    }
                }
            }
            return active;
        }

        public ActionResult<PracticeDTO> UpdatePracticeStatus(int id,int activeInd)
        {
            
              PracticeDTO practiceDTO = new PracticeDTO();

            using (SqlConnection conn = new SqlConnection(ConnString))
            {
                using (SqlCommand cmd = new SqlCommand(updatePracticeStatus, conn))
                {
                    cmd.Parameters.Add("@id", SqlDbType.VarChar).Value = id;
                    cmd.Parameters.Add("@activeInd", SqlDbType.VarChar).Value = activeInd;
                    cmd.CommandType = CommandType.StoredProcedure;
                    conn.Open();
                    using (SqlDataReader reader = cmd.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            MapReaderToDTO(reader, practiceDTO);
                        }
                    }
                }
            }
            return practiceDTO;
        }
    }
}
